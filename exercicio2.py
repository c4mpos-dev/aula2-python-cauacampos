class Conta:
    def __init__(self, numero_conta, nome_correntista, saldo=0):
        self.numero_conta = numero_conta
        self.nome_correntista = nome_correntista
        self.saldo = saldo

    def alterar_nome(self, novo_nome):
        self.nome_correntista = novo_nome
        print("\nNome alterado com sucesso.")

    def depositar(self, valor):
        self.saldo += valor
        print("\nDepósito realizado com sucesso.")

    def sacar(self, valor):
        if self.saldo >= valor:
            self.saldo -= valor
            print("\nSaque realizado com sucesso.")
        else:
            print("\nSaldo insuficiente.")

numero_conta = input("Digite o número da conta: ")
nome_correntista = input("Digite o nome do correntista: ")
saldo_inicial = float(input("Digite o saldo inicial: "))
conta = Conta(numero_conta, nome_correntista, saldo_inicial)

def exibir_menu():
    print("\nMenu:\n[1]Consultar saldo\n[2]Depositar\n[3]Sacar\n[4]Alterar nome do correntista\n[5]Sair")

while True:
    exibir_menu()
    opcao = input(f"{conta.nome_correntista}, escolha uma opção: ")

    if opcao == "1":
        print(f"\nSaldo atual: R${conta.saldo}")
    elif opcao == "2":
        valor = float(input("Digite o valor a ser depositado: "))
        conta.depositar(valor)
    elif opcao == "3":
        valor = float(input("Digite o valor a ser sacado: "))
        conta.sacar(valor)
    elif opcao == "4":
        novo_nome = input("Digite o novo nome do correntista: ")
        conta.alterar_nome(novo_nome)
    elif opcao == "5":
        print("Saindo da conta...")
        break