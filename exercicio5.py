from classes import Carro
from classes import Loja


def main():
    carros = [
        Carro('Audi', 'Q3', 180000),
        Carro('Ferrari', 'Italia 458', 1500000),
        Carro('FIAT', 'FastBack', 120000)
    ]

    loja = Loja(carros)

    print("Bem-vindo(a) à nossa loja!\n")

    while True:
        print("\nO que deseja fazer? ")
        print("[1] Comprar carro\n"
              "[2] Adicionar carro\n"
              "[3] Ver carros\n"
              "[4] Sair da loja\n"
              "\nEscolha uma opção: ")

        choice = int(input())

        if choice == 1:
            loja.comprar_carro()
        elif choice == 2:
            loja.adicionar_carro()
        elif choice == 3:
            loja.mostrar_carros()
        elif choice == 4:
            break
        else:
            print("\nOpção inválida! Tente novamente.")


main()