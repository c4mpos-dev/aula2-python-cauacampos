# class Aluno:
#
#     def __init__(self, nome, idade):
#         self.nome = nome
#         self.idade = idade
#         self.nota1 = 0
#         self.nota2 = 0
#
#     def addNotas(self, nota1, nota2):
#         self.nota1 = nota1
#         self.nota2 = nota2
#
#     def media(self):
#         return (self.nota1 + self.nota2)/2
#
#     def resultado(self):
#         if self.media() >= 6.0:
#             return 'Aprovado'
#         elif self.media() < 4.0:
#             return 'Reprovado'
#         else:
#             return 'NP3'
#
# #aluno1 = Aluno('Cauã', 17)
# #print(aluno1.nome, aluno1.idade)
#
# aluno2 = Aluno(nome="João", idade=25)
# aluno2.addNotas(nota1=10, nota2=7)
# print(aluno2.nota1, aluno2.nota2, aluno2.media(), aluno2.resultado())
#
# print(dir(Aluno))

#--------------------- HERANÇA -----------------------#

# class Animal(object):
#     pass
#
# class Ave(Animal):
#     pass
#
# class Aguia(Ave):
#     pass


# class Pai(object):
#     nome = "Ronaldo"
#     sobrenome = 'Hassum'
#     cidade = "Piraí"
#     olhos = "azuis"
#
# class Filha(Pai):
#     nome = 'Julia'
#     olhos = 'verdes'
#
# class Neta(Filha):
#     nome = "Giulia"
#
# print(Pai.nome, Filha.nome, Neta.nome)
# print(Pai.cidade, Filha.cidade, Neta.cidade)
# print(Pai.olhos, Filha.olhos, Neta.olhos)
# print(Pai.sobrenome, Filha.sobrenome, Neta.sobrenome)

# class Pessoa(object):
#     nome = None
#     idade = None
#
#     def __init__(self, nome, idade):
#         self.nome = nome
#         self.idade = idade
#
#     def envelhecer(self):
#         self.idade += 1
#
# class Atleta(Pessoa):
#     peso = None
#     aposentado = None
#
#     def __init__(self, nome, idade, peso):
#         super().__init__(nome, idade)
#         self.peso = peso
#
#     def aquecer(self):
#         print('Atleta aquecido!')
#
#     def aposentar(self):
#         self.aposentado = True
#
# class Corredor(Atleta):
#     def correr(self):
#         print('Correndo!!!')
#
# class Nadador(Atleta):
#     def nadar(self):
#         print('Nadando!!!')
#
# class Ciclista(Atleta):
#     def pedalar(self):
#         print('Pedalando!!!')
#
# corredor_1 = Corredor('Cauã', 35, 70)
# nadador_1 = Nadador('Jorge', 40, 75)
# ciclista_1 = Ciclista("Saulo", 50, 80)
#
# corredor_1.aquecer()
# nadador_1.aquecer()
# ciclista_1.aquecer()
#
# print(ciclista_1.aposentado, ciclista_1.idade)
# ciclista_1.aposentar()
# ciclista_1.envelhecer()
# print(ciclista_1.aposentado, ciclista_1.idade)

# ------------------ decoradores ---------------------#

# def decorador(func):
#     def wrapper():
#         print('Antes de executar')
#         func()
#         print('Depois de executar')
#     return wrapper
#
# @decorador
# def media_da_turma():
#     nota_1 = 10
#     nota_2 = 5
#     print(f'Média = {(nota_1 + nota_2) / 2}')
#
# media_da_turma()

from classes import CalculoImposto

calc = CalculoImposto()

calc.taxa = 55.25
print(calc.taxa)

calc.taxa = 105.60
print(calc.taxa)

