#-------------------- EXEMPLO MAIN ------------------#

class CalculoImposto:
    def __init__(self):
        self.__taxa = 0

    @property
    def taxa(self):
        return round(self.__taxa, 2)

    @taxa.setter
    def taxa(self, val__taxa):
        if val__taxa < 0:
            self.__taxa = 0
        elif val__taxa > 100:
            self.__taxa = 100
        else:
            self.__taxa = val__taxa


#------------------- EXERCICIO 4 ------------------#


class ControlarDesconto:
    def __init__(self, preco_produto, porcentagem_desconto):
        self.preco_produto = preco_produto
        self.__porcentagem_desconto = self.validar_porcentagem(porcentagem_desconto)

    @property
    def porcentagem_desconto(self):
        return self.__porcentagem_desconto

    @porcentagem_desconto.setter
    def porcentagem_desconto(self, valor):
        self.__porcentagem_desconto = self.validar_porcentagem(valor)

    def validar_porcentagem(self, porcentagem):
        if porcentagem < 1 or porcentagem > 35:
            return 1
        else:
            return int(porcentagem)

    def aplicar_desconto(self):
        valor_desconto = (self.porcentagem_desconto / 100) * self.preco_produto
        preco_com_desconto = self.preco_produto - valor_desconto
        return preco_com_desconto


#-------------------- EXERCICIO 5 ------------------#


class Carro:
    def __init__(self, marca, modelo, preco):
        self.marca = marca
        self.modelo = modelo
        self.preco = preco


class Loja:
    def __init__(self, carros):
        self.carros = carros

    def mostrar_carros(self):
        print("Carros disponíveis na loja:\n")
        for i, carro in enumerate(self.carros):
            print(f"{i} - {carro.marca} | {carro.modelo} | R${carro.preco}")

    def comprar_carro(self):
        self.mostrar_carros()
        numero_carro = int(input("\nDigite o número do carro para comprar: "))
        if 0 <= numero_carro < len(self.carros):
            carro_comprado = self.carros.pop(numero_carro)
            print(
                f"\nVocê comprou o carro {carro_comprado.marca} | {carro_comprado.modelo} por R${carro_comprado.preco}")
        else:
            print("\nNúmero de carro inválido!")

    def adicionar_carro(self):
        marca = input("\nDigite a marca do carro: ")
        modelo = input("Digite o modelo do carro: ")
        preco = float(input("Digite o preço do carro: "))
        novo_carro = Carro(marca, modelo, preco)
        self.carros.append(novo_carro)
        print("\nCarro adicionado com sucesso!")
