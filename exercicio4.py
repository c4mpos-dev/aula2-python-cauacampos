from classes import ControlarDesconto

preco = float(input("Digite o preço do produto: "))
desconto = float(input("Digite a porcentagem de desconto (entre 1 e 35): "))

desconto_produto = ControlarDesconto(preco, desconto)

print(f"Porcentagem de desconto: {desconto_produto.porcentagem_desconto}%")
print(f"Valor do produto com desconto: R$ {round(desconto_produto.aplicar_desconto(), 2)}")
