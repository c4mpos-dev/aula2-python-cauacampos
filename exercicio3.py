class Pessoa(object):

    def __init__(self, nome, idade, sexo):
        self.nome = nome
        self.idade = idade
        self.sexo = sexo


class Cidadao(Pessoa):

    def __init__(self, nome, idade, sexo, cpf):
        super().__init__(nome, idade, sexo)
        self.cpf = cpf


cidadao = Cidadao("Roberto", 45, "Masculino", "32189461")
print(cidadao.nome, cidadao.idade, cidadao.sexo, cidadao.cpf)
